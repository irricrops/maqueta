var mantencion = angular.module('mantencion', ['ngMaterial', 'ngAnimate', 'ngMessages', 'ngAria', 'ui.router', 'ngWig']);

(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/initial');

        $stateProvider


        .state('initial', {
            url: '/initial',
            templateUrl: 'partials/initial-partial.html',
            controller: 'InitialController',
            controllerAs : "i"
        })



        .state('equipo', {
            url: '/equipo',
            templateUrl: 'partials/equipo-partial.html',
            controller: 'EquipoController',
            controllerAs : "e"
        })


        .state('task', {
            url: '/task',
            templateUrl: 'partials/task-details-partial.html',
            controller: 'TaskController',
            controllerAs : "a"
        })

        .state('pieza', {
            url: '/pieza',
            templateUrl: 'partials/pieza-partial.html',
            controller: 'PiezaController',
            controllerAs : "p"
        });



    }]);


    app.config(["$mdThemingProvider", function($mdThemingProvider) {


  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey', {
      'default': '700', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '300', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '800', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    // If you specify less than all of the keys, it will inherit from the
    // default shades
    .accentPalette('cyan', {
      'default': '800' // use shade 200 for default, and keep all other shades the same
    });

  }]);

})(mantencion);
