(function(app) {
	app.controller('sidenavController', ['$scope', function($scope) {

		var vm = this;

		vm.periodos = [
			{
				name:"Hoy"
			},
			{
				name:"Semana"
			},
			{
				name:"Mes"
			},
		];

		vm.periodoSelected = vm.periodos[0];





		vm.tareas = [];
		vm.fallas = [];

		for (var i = 0; i < 5; i++) {
			vm.tareas.push(
				{
					id       : _.random(1, 100000),
					equipo   :"equipo 1",
					tarea    :"Revisión de bandas",
					encargado:"Cristian torres",
					//prioridad: _.random(1, 3),
					horas: 5
				}
			);
			vm.fallas.push(
				{
					id       : _.random(1, 100000),
					equipo   :"equipo 1",
					tarea    :"Problemas con las bandas",
					encargado:"Cristian torres",
				}
			);
		}

		vm.tareas = _.orderBy(vm.tareas , ['prioridad'],['asc']);

		vm.tareaSelected = vm.tareas[0];
		vm.fallaSelected = vm.fallas[0];

		vm.tareasDone = [];

		for (var i = 0; i < 5; i++) {
			vm.tareasDone.push(
				{
					id       : _.random(1, 100000),
					equipo   :"equipo 1",
					tarea    :"Falla en motor",
					encargado:"Cristian torres",
					prioridad: _.random(1, 3),
					hecho    : Boolean(_.random(0, 1)),
					//correctiva : Boolean(_.random(0, 1)),
				}
			);
		}













	}]);
})(mantencion);
