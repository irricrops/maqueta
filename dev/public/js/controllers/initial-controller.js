(function(app) {
	app.controller('InitialController', ['$scope', function($scope) {
		var vm = this;
		vm.test = "test";

		vm.locations = [];

		for (var i = 0; i < 10; i++) {
			vm.locations.push({
				num      : (i + 1),
				nombre   : "Bodega B"+(i+1),
				position : {
					x: 120 * (_.floor(i/4) + 1),
					y: 120 * (i%4 + 0.5)
				}
			});
		}

		vm.locationSelected = vm.locations[0];

	}])






	.directive('ngDraggable', ['$document', '$timeout', '$state', function($document, $timeout, $state){
		return {
			restrict: 'A',
			scope: {
				dragOptions: '=ngDraggable',
				item: '=ngDraggableItem',
				disabled: '=disabled'
			},
			link: function(scope, elem, attr) {
				var startX, startY, x = 0, y = 0, item = null,
				start, stop, drag, selected, container, id, resizing = false;

				var width  = elem[0].offsetWidth,
				height = elem[0].offsetHeight;

				var elementSelected = null;

				// Obtain drag options
				if (scope.dragOptions) {
					start  = scope.dragOptions.events.start;
					drag   = scope.dragOptions.events.drag;
					stop   = scope.dragOptions.events.stop;
					selected = scope.dragOptions.events.selected;
					var css = scope.dragOptions.style;
					$(elem).css(css);
				}

				/*
				scope.dragOptions.api = {
					select: function(){
						$(".box").removeClass("selected");
						$(elem).addClass("selected");
						if(selected) selected(null, item);
					},
					unselect: function(){
						$(".box").removeClass("selected");
						if(selected) selected(null, null);
					}
				}
				*/

				if(scope.item){
					item = scope.item;
				}



				// Bind mousedown event
				elem.on('mousedown', function(e) {
					if(scope.disabled) {
						$state.go("task");
						return null;
					}
					if(resizing) return null;

					e.preventDefault();

					id     = scope.dragOptions.container;
					width  = elem[0].offsetWidth;
					height = elem[0].offsetHeight;

					if (id) {
						container = document.getElementById(id).getBoundingClientRect();
						$("#"+id).unbind("click").on("click", function(e){
							if(e.toElement != this) return;
							$(".box").removeClass("selected");
							if(selected) selected(e, null);
						});
					}

					$(".box").removeClass("selected");
					$(elem).addClass("selected");
					if(selected) selected(e, item);

					startX = e.clientX - elem[0].offsetLeft;
					startY = e.clientY - elem[0].offsetTop;
					$document.on('mousemove', mousemove);
					$document.on('mouseup', mouseup);
					if (start) start(e);
				});

				// Handle drag event
				function mousemove(e) {
					y = e.clientY - startY;
					x = e.clientX - startX;
					setPosition();
					if (drag) drag(e);
				}

				// Unbind drag events
				function mouseup(e) {
					$document.unbind('mousemove', mousemove);
					$document.unbind('mouseup', mouseup);
					refreshDragOptions();
					if (stop) stop(e);
				}

				// Move element, within container if provided
				function setPosition() {
					if (container) {
						if (x < container.left) {
							x = container.left;
						} else if (x > container.right - width) {
							x = container.right - width;
						}
						if (y < container.top) {
							y = container.top;
						} else if (y > container.bottom - height) {
							y = container.bottom - height;
						}
					}

					elem.css({
						top: y + 'px',
						left:  x + 'px'
					});
				}

				function refreshDragOptions(){
					scope.dragOptions.style.width  = parseFloat(elem[0].style.width);
					scope.dragOptions.style.height = parseFloat(elem[0].style.height);
					scope.dragOptions.style.top    = parseFloat(elem[0].style.top);
					scope.dragOptions.style.left   = parseFloat(elem[0].style.left);
				}





				var makeResizable = function(element){
					//var element        = document.getElementById(id);
					var resizer          = document.createElement('div');
					resizer.className    = 'resizer';
					resizer.style.cursor = 'se-resize';
					$(element).append(resizer);
					resizer.addEventListener('mousedown', initResize, false);

					function initResize(e) {
						if(scope.disabled) return;
						window.addEventListener('mousemove', Resize, false);
						window.addEventListener('mouseup', stopResize, false);
						resizing = true;
					}
					function Resize(e) {
						var id               = scope.dragOptions.container;
						var elemContainer    = document.getElementById(id).parentNode;
						element.style.width  = (e.clientX - element.offsetLeft - elemContainer.offsetLeft) + 'px';
						element.style.height = (e.clientY - element.offsetTop - elemContainer.offsetTop) + 'px';
					}
					function stopResize(e) {
						window.removeEventListener('mousemove', Resize, false);
						window.removeEventListener('mouseup', stopResize, false);
						refreshDragOptions();
						resizing = false;
					}
				}

				makeResizable(elem[0]);




			}
		}

	}])


	// ---------------------------------------------
	// ---------------------------------------------
	// ---------------------------------------------
	// ---------------------------------------------



	.controller('AppCtrl', ['$scope', '$timeout', function($scope, $timeout) {
		var eventsDO = {
			start: function(e) {
				$scope.spanLog = "STARTING";
			},
			drag: function(e) {
				$scope.spanLog = "DRAGGING";
			},
			stop: function(e) {
				$scope.spanLog = "STOPPING";
			},
			selected : function(e, item){
				$timeout(function(){
					$scope.boxSelected = item
				});
			},
		};

		$scope.positions = [
			{
				id: 1,
				name: "Arriba",
				className: "label-top"
			},
			{
				id: 2,
				name: "Derecha",
				className: "label-right"
			},
			{
				id:3,
				name: "Abajo",
				className: "label-bottom"
			},
			{
				id: 4,
				name: "Izquierda",
				className: "label-left"
			},
			{
				id: 5,
				name: "interno",
				className: "label-inside"
			},
		];

		$scope.spanLog = "initial";
		//$scope.boxes = [];
		$scope.boxes = [{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":266,"top":115,"width":22,"height":48},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":252,"top":71,"width":50,"height":40},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Cinta","position":{"id":3,"name":"Abajo","className":"label-bottom"},"dragOptions":{"style":{"left":419,"top":62,"width":16,"height":151},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":257,"top":311,"width":39,"height":45},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":255,"top":166,"width":49,"height":39},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":267,"top":211,"width":22,"height":50},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":256,"top":265,"width":46,"height":44},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":307,"top":61,"width":63,"height":56},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":375,"top":80,"width":37,"height":20},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":439,"top":83,"width":172,"height":16},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":2,"name":"Derecha","className":"label-right"},"dragOptions":{"style":{"left":557,"top":102,"width":40,"height":121},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":2,"name":"Derecha","className":"label-right"},"dragOptions":{"style":{"left":535,"top":226,"width":91,"height":23},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":2,"name":"Derecha","className":"label-right"},"dragOptions":{"style":{"left":558,"top":252,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"test","position":{"id":3,"name":"Abajo","className":"label-bottom"},"dragOptions":{"style":{"left":222,"top":357,"width":122,"height":9},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Impresora","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":144,"top":292,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":168,"top":551,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":179,"top":510,"width":16,"height":34},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":279,"top":426,"width":34,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Paletiado","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":231,"top":419,"width":34,"height":25},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Caja","position":{"id":2,"name":"Derecha","className":"label-right"},"dragOptions":{"style":{"left":342,"top":483,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Detector","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":170,"top":475,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"Bolsa","position":{"id":2,"name":"Derecha","className":"label-right"},"dragOptions":{"style":{"left":355,"top":383,"width":15,"height":93},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":181,"top":389,"width":13,"height":83},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"","position":{"id":1,"name":"Arriba","className":"label-top"},"dragOptions":{"style":{"left":346,"top":346,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}},{"name":"cierre de caja","position":{"id":4,"name":"Izquierda","className":"label-left"},"dragOptions":{"style":{"left":178,"top":354,"width":40,"height":30},"events":{},"container":"map-container"},"values":{"alert":5}}]
		$scope.editing = false;

		//lee los box desde localStorage
		if(localStorage.getItem('boxes')){
			$scope.boxes = JSON.parse(localStorage.getItem('boxes'));
		}

		angular.forEach($scope.boxes, function(box){
			box.dragOptions.events = eventsDO;
			//rellena los datos posiblemente faltantes
			//if(box.position === undefined) box.position = $scope.positions[0];
			//if(box.values === undefined) box.values = {alert: 5}
		});

		$scope.boxSelected = null;

		$scope.addElement = function(){
			$timeout(function(){
				$scope.boxes.push({
					name: "test",
					position : $scope.positions[0],
					values:{
						alert: 5
					},
					dragOptions:{
						style:{
							left  : 130,
							top   : 150,
							width : 40,
							height: 30
						},
						events   : eventsDO,
						container: 'map-container',
					}
				});
			});
		}

		$scope.printElements = function(){
			console.dir($scope.boxes);
			var boxes = angular.copy($scope.boxes);
			angular.forEach(boxes, function(box){
				delete box.dragOptions.events;
			});
			//escribe los box desde localStorage
			localStorage.setItem('boxes', angular.toJson($scope.boxes));
		}

		$scope.cleanAll = function(){
			$scope.boxes = [];
			$scope.boxSelected = null;
		}

		$scope.duplicate = function(box){
			var newBox = angular.copy(box);
			newBox.dragOptions.style.top += 10;
			newBox.dragOptions.style.left+= 10;
			$scope.boxes.push(newBox);
			$scope.boxSelected = newBox;
			//newBox.dragOptions.api.select();
		}

		$scope.delete = function(box){
			var index = $scope.boxes.indexOf(box);
			$scope.boxes.splice(index, 1);
			$scope.boxSelected = null;
		}




		if($scope.boxes.length == 0){
			$scope.addElement();
		}





	}]);



})(mantencion);
