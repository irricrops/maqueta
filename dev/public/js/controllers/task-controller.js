(function(app) {
	app.controller('TaskController', ['$scope', '$mdDialog', function($scope, $mdDialog) {

		var vm       = this;
		vm.items     = [];
		vm.tipoTarea = 2;

		vm.equipo = {
			nombre       : "equipo 1",
			id           : 1234,
		}

		vm.tarea = {
			edit         : false,
			horas        : 5,
			repeticion   : 7,
			nombre       : "Problema con banda",
			description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			tareas       : [],
			instructions : [],
			pieces       : [{
				equipo   :"equipo 1",
				nombre   :"nombre de la pieza",
			},
			{
				equipo   :"equipo 1",
				nombre   :"nombre de la parte",
			}],
		}

		for (var i = 0; i < 4; i++) {
			vm.tarea.tareas.push(
				{
					equipo     :"equipo 1",
					tarea      :"Revisión de banda",
					encargado  :"Cristian torres",
					hecho      : Boolean(_.random(0, 1)),
					correctiva : Boolean(_.random(0, 1)),
				}
			);
		}

		for (var i = 0; i < 3; i++) {
			vm.tarea.instructions.push(
				{
					name        :"instruccion "+ (i + 1),
					edit        : false,
					description :"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					ready       : false,
				}
			);
		}

		vm.comments = [];
		for (var i = 0; i < 2; i++) {
			vm.comments.push({
				user: "Cristian Torres",
				comment: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
			})
		}


		vm.openMenu = function($mdMenu, ev) {
      originatorEv = ev;
      $mdMenu.open(ev);
    };

		vm.edit = function(tarea){
			tarea.edit = true;
		}

		vm.save = function(tarea){
			tarea.edit = false;
		}


		vm.delete = function(tarea){
			tarea.edit = false;
		}

		vm.cancel = function(tarea){
			tarea.edit = false;
		}





		vm.editInstruction = function(ins){
			ins.edit = true;
		}

		vm.saveInstruction = function(ins){
			ins.edit = false;
		}

		vm.deleteInstruction = function(ins, ev){

			var confirm = $mdDialog.confirm()
			.title('¿Realmente desea eliminar esta instruccion?')
			.textContent('Este cambio es permanente')
			.ariaLabel('Delete')
			.targetEvent(ev)
			.ok('Eliminar!')
			.cancel('Cancelar');

			$mdDialog.show(confirm).then(function() {
				var index = vm.tarea.instructions.indexOf(ins);
				vm.tarea.instructions.splice(index, 1);
			}, function() {
				//
			});
		}

		vm.cancelInstruction = function(ins){
			ins.edit = false;
		}

		vm.addInstruction = function(){
			vm.tarea.instructions.push(
				{
					name        :"nombre de la instrucción ",
					edit        : true,
					description :"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					ready       : false,
				}
			);
		}




		vm.images = [
			{
				'url': 'https://farm6.staticflickr.com/5830/20552523531_e1efec8d49_k.jpg',
				'caption': 'This image has dimensions 2048x1519 and the img element is scaled to fit inside the window.'
			}
		];
		vm.imageSelected = null;

		vm.lbClose = function(){
			vm.imageSelected =null;
		}

		vm.lbImage = function(image){
			vm.imageSelected = image;
		}



	}]);
})(mantencion);
