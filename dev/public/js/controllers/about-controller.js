(function(app) {
	app.controller('AboutController', ['$scope', function($scope) {

		var vm = this;
		vm.items = [];

		for (var i = 0; i < 4; i++) {
			vm.items.push(
				{
					equipo   :"equipo 1",
					tarea    :"nombre de la tarea",
					encargado:"Cristian torres",
				}
			);
		}

		vm.pieces = [];

		for (var i = 0; i < 2; i++) {
			vm.pieces.push(
				{
					equipo   :"equipo 1",
					nombre   :"nombre de la pieza",
				}
			);
		}


	}]);
})(mantencion);
