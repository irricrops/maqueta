(function(app) {
	app.controller('EquipoController', ['$scope', function($scope) {
		var vm = this;

		vm.equipos= [];

		vm.equipo = {
			nombre     : "Equipo 1",
			edit       : false,
			encargado  :"cristian torres",
			bodega     : "Galpon 1",
			items      : [],
			itemsReady : [],
			piezas     : []
		}


		for (var i = 0; i < 4; i++) {
			vm.equipos.push(
				{
					equipo    :"equipo " + (i + 1),
					encargado :"cristian torres",
				}
			);
		}
		vm.equipoSelected = vm.equipos[0];




		for (var i = 0; i < 4; i++) {
			vm.equipo.items.push(
				{
					equipo   :"equipo 1",
					tarea    :"nom tarea",
					encargado:"cristian torres",
					prioridad: _.random(1, 3),
					hecho    : Boolean(_.random(0, 1)),
					correctiva : Boolean(_.random(0, 1)),
				}
			);
		}


		for (var i = 0; i < 4; i++) {
			vm.equipo.itemsReady.push(
				{
					equipo   :"equipo 1",
					tarea    :"nom tarea",
					encargado:"cristian torres",
					prioridad: _.random(1, 3),
					hecho    : Boolean(_.random(0, 1)),
					correctiva : Boolean(_.random(0, 1)),
				}
			);
		}


		var miniLorem = [
			"Lorem ipsum dolor sit amet, et dolore magna aliqua.",
			"Ut enim ad minim veniam, quiequat. Duis aute irure dolor in reprehenderit in ",
			"voluptate velit esse cillum oident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"Excepteur sint occaecat cupi laborum."
		];

		var lorem = [
			"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
			"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ",
			"voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		];

		for (var i = 0; i < 10; i++) {
			vm.equipo.piezas.push(
				{
					num: i+1,
					position: {
						x: 50 * (_.floor(i/4) + 1),
						y: 60 * (i%4 + 0.5)
					},
					nombre  :"parte " + (i+1),
					minDesc : miniLorem[i%4],
					desc    : lorem[i%4],
					edit: false,
				}
			);
		};
		vm.piezaSelected = vm.equipo.piezas[0];


		vm.edit = function(equipo){
			equipo.edit = true;
		}

		vm.save = function(equipo){
			equipo.edit = false;
		}


		vm.delete = function(equipo){
			equipo.edit = false;
		}

		vm.cancel = function(equipo){
			equipo.edit = false;
		}








	}]);
})(mantencion);
