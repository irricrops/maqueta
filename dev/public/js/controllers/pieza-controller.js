(function(app) {
	app.controller('PiezaController', ['$scope', function($scope) {

		var vm     = this;
		vm.equipos = [];


		vm.pieza = {
			nombre : "Pieza ejemplo",
			edit   : false,
			codigo : "ASD123",
		};

		var miniLorem = [
			"Lorem ipsum dolor sit amet, et dolore magna aliqua.",
			"Ut enim ad minim veniam, quiequat. Duis aute irure dolor in reprehenderit in ",
			"voluptate velit esse cillum oident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"Excepteur sint occaecat cupi laborum."
		];

		var lorem = [
			"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
			"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ",
			"voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
			"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		];


		for (var i = 0; i < 5; i++) {
				var e = {
				nombre : "equipo "+ ( i +1 ),
				piezas :[]
			};
			for (var j = 0; j < 10; j++) {
				e.piezas.push({
					nombre : "pieza "+ ( j +1),
				});
			}
			vm.equipos.push(e);
		}

		vm.piezasImagen = [];
			for (var j = 0; j < 15; j++) {
				vm.piezasImagen.push({
					num      : (j+1),
					nombre   : "elemento "+ ( j +1),
					minDesc  : miniLorem[j%4],
					desc     : lorem[j%4],
					position : {
						x: 50 * (_.floor(j/4) + 1),
						y: 60 * (j%4 + 0.5)
					},
				});
			}


		vm.items = [];

		for (var i = 0; i < 4; i++) {
			vm.items.push(
				{
					equipo     :"equipo 1",
					pieza      :"pieza 1",
					tarea      :"nom tarea",
					encargado  :"cristian torres",
					prioridad  : _.random(1, 3),
					hecho      : Boolean(_.random(0, 1)),
					correctiva : Boolean(_.random(0, 1)),
				}
			);
		}

		vm.itemsReady = [];

		for (var i = 0; i < 4; i++) {
			vm.itemsReady.push(
				{
					equipo     :"equipo 1",
					pieza      :"pieza 1",
					tarea      :"nom tarea",
					encargado  :"cristian torres",
					prioridad  : _.random(1, 3),
					hecho      : Boolean(_.random(0, 1)),
					correctiva : Boolean(_.random(0, 1)),
				}
			);
		}

		vm.equipoSelected      = vm.equipos[0];
		vm.piezaSelected       = vm.equipos[0].piezas[0];
		vm.piezaImagenSelected = vm.piezasImagen[0];



		vm.edit = function(pieza){
			pieza.edit = true;
		}

		vm.save = function(pieza){
			pieza.edit = false;
		}


		vm.delete = function(pieza){
			pieza.edit = false;
		}

		vm.cancel = function(pieza){
			pieza.edit = false;
		}



	}]);
})(mantencion);
