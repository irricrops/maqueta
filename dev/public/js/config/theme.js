(function(app) {
	app
    .config(function($mdThemingProvider) {
      $mdThemingProvider.theme('default')
        .primaryPalette('blue-grey')
        .accentPalette('cyan');
    })
  })(mantencion);
